import React, {Suspense} from 'react';
import HeaderComponent from './components/HeaderComponent';
import Information from './components/Information';

function App() {
  return (
    <Suspense fallback="loading">
      <div>
        <HeaderComponent/>
        <Information />
      </div>
    </Suspense>
  );
}

export default App;
