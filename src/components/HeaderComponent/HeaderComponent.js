import React from 'react';
import {useTranslation} from "react-i18next";
import './Header.css';
import LangButtons from '../LangButtons';

const HeaderComponent = () => {
  const { t } = useTranslation('common');
  return (
    <div className="header__container">
      <span className="header__title-container">
        <h1>{t('welcome.title', {framework:'React'})}</h1>
        <p>{t('welcome.subtitle')}</p>
      </span>      
      <LangButtons />
    </div>
  )
}

export default HeaderComponent;