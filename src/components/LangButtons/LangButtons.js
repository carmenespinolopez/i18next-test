import React from 'react';
import {useTranslation} from "react-i18next";
import './LangButtons.css';
import Button from '@material-ui/core/Button';

export default function LangButtons() {
  const { i18n } = useTranslation('common');

  const english = () => {
    i18n.changeLanguage('en');
    document.getElementById("enButton").disabled = true;
    document.getElementById("esButton").removeAttribute('disabled');
  }

  const spanish = () => {
    i18n.changeLanguage('es');
    document.getElementById("esButton").disabled = true;
    document.getElementById("enButton").removeAttribute('disabled');
  }

  return (
    <div className="langButtons__container">
      <Button color="primary" variant="contained" id="esButton" onClick={spanish}>es</Button>
      <Button color="secondary" variant="contained" id="enButton" onClick={english}>en</Button>
    </div>
  )
}
