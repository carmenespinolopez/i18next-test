import React from 'react';
import {useTranslation} from "react-i18next";
import './Information.css';

const Information = () => {
  const { t } = useTranslation('common');
  return (
    <div className="main__container">
      <h2 className="main__title">{t('information.title', {framework:'React'})}</h2>
      <p>{t('information.body')}</p>
      <ul className="main__list">
        <li>
          <a className="main__links" href="https://www.i18next.com/">
            i18next
          </a>          
        </li>
        <li>
          <a className="main__links" href="https://www.codeandweb.com/babeledit/tutorials/how-to-translate-your-react-app-with-react-i18next">
            Codeandweb tutorial
          </a>
        </li>
      </ul>
      <img src="https://media.giphy.com/media/LmNwrBhejkK9EFP504/giphy.gif" alt="cat-coding" />
    </div>
  )
}

export default Information;
