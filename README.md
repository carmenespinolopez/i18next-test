# i18next application

Application build with React to test i18next which is an internationalization-framework.
Translation between English and Spanish.

## Installation
- Clone the repository
```sh
git clone <link>
```
- Go to the location of the cloned repository
```sh
cd i18next-test
```
- Install the dependencies and start the application
```sh
npm i
npm start
```
- The application will automatically open at `http://localhost:3000/`.
- ✨ Magic ✨


### More information:
- [Official i18next page](https://www.i18next.com/)
- [Codeandweb tutorial](https://www.codeandweb.com/babeledit/tutorials/how-to-translate-your-react-app-with-react-i18next)
